import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import store from './src/store';
import 'babel-polyfill';
import 'babel-core/register';

ReactDOM.render(
  <Provider store={store} />,
  document.getElementById('app'),
);
